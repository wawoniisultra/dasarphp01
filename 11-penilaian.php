<?php
  /*
    #Mencari Total Nilai dan Grade pada PHP
  */

  //menampung Nilai dari textfield ke dalam variabel
  $nim = @$_POST['tnim'];
  $nama = @$_POST['tnama'];
  $jurusan = @$_POST['tjurusan'];
  $tugas = @$_POST['ttugas'];
  $quis = @$_POST['tquis'];
  $uts = @$_POST['tuts'];
  $uas = @$_POST['tuas'];
  $total = @$_POST['ttotal'];
  $grade = @$_POST['tgrade'];


  //Pengujian jika Field Nim di enter
  if(isset($nim))
  {
  	if($nim == "15.50.001"){
  		$nama = "Muhammad Uwais";
  		$jurusan = "Sistem Informasi";
  	}elseif($nim == "15.51.001"){
  		$nama = "Muhammad Hafid";
  		$jurusan = "Teknik Informatika";
  	}else{
  		echo "<script>alert('Maad, Data tidak ditemukan..!')</script>";
  		$nama = "";
  		$jurusan = "";
  	}
  	
  }

  //Pengujian jika tombol proses diklik
  if(isset($_POST['bproses'])){

  	//menampung total nilai
  	$total = ($tugas + $quis + $uts + $uas) / 4;

  	//Pengujian Total Nilai untuk mendapatkan Grade
  	if($total >= 90){
  		$grade = "A";
  	}elseif($total >= 80 && $total < 90){
  		$grade = "AB";
  	}elseif($total >= 70 && $total < 80){
  		$grade = "B";
  	}elseif($total >= 60 && $total < 70){
  		$grade = "BC";
  	}elseif($total >= 50 && $total < 60){
  		$grade = "C";
  	}elseif($total >= 40 && $total < 50){
  		$grade = "D";
  	}else{
  		$grade = "E";
  	}

  }




?>

<!DOCTYPE html>
<html>
<head>
	<title>Penilaian Mahasiswa Ngoding pintar</title>
</head>
<body>

<center><h2>Penilaian Mahasiswa Ngoding Pintar</h2></center>
<form method="post" action="">
	

<table border="0" align="center">
	<tr>
		<td colspan="3"> <hr> </td>
	</tr>
	<tr>
		<td>Nim</td>
		<td>:</td>
		<td>
			<input type="text" name="tnim" size="10" value="<?=$nim?>">
		</td>
	</tr>
	<tr>
		<td>Nama</td>
		<td>:</td>
		<td>
			<input type="text" name="tnama" value="<?=$nama?>">
		</td>
	</tr>
	<tr>
		<td>Jurusan</td>
		<td>:</td>
		<td>
			<input type="text" name="tjurusan" value="<?=$jurusan?>">
		</td>
	</tr>
	<tr>
		<td>Nilai Tugas</td>
		<td>:</td>
		<td>
			<input type="text" name="ttugas" size="5" value="<?=$tugas?>">
		</td>
	</tr>
	<tr>
		<td>Nilai Quis</td>
		<td>:</td>
		<td>
			<input type="text" name="tquis" size="5" value="<?=$quis?>">
		</td>
	</tr>
	<tr>
		<td>Nilai UTS</td>
		<td>:</td>
		<td>
			<input type="text" name="tuts" size="5" value="<?=$uts?>">
		</td>
	</tr>
	<tr>
		<td>Nilai UAS</td>
		<td>:</td>
		<td>
			<input type="text" name="tuas" size="5" value="<?=$uas?>">
		</td>
	</tr>
	<tr>
		<td colspan="3"> <hr> </td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>
			<input type="submit" name="bproses" value="Proses">
		</td>
	</tr>
	<tr>
		<td>Total Nilai</td>
		<td>:</td>
		<td>
			<input type="text" name="ttotal" size="5" value="<?=$total?>">
		</td>
	</tr>
	<tr>
		<td>Grade</td>
		<td>:</td>
		<td>
			<input type="text" name="tgrade" size="5" value="<?=$grade?>">
		</td>
	</tr>
	<tr>
		<td colspan="3"> <hr> </td>
	</tr>

</table>

</form>

</body>
</html>
